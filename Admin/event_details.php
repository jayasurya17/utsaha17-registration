<html>
<head>
<title>
	Utsaha - <?php echo $_GET['name'] ?>
</title>
<?php include("../bootstrap_header.html");?>
</head>
<body>
		<?php
			error_reporting(0);
			session_start();
			if(!isset($_SESSION['usrname']) || !isset($_SESSION['super']) || $_SESSION['super'] !=1 )
			{
				header('Location:/');
			}
		?>
	<h1><?php echo $_GET['name'] ?></h1><hr>
	<div style="text-align:left">
	        <h3>Event heads please ensure that the following are done</h3>
       		<ul>
	            <li>Verify the access code or college ID card of participants before allowing them inside.</li>
	            <li>Mark a tick in each participant's row to indicate that they have participated.</li>
	            <li>Sign at the bottom of this report with your name.</li>
	            <li>Please return these sheets to Jayasurya(8197360630) or Abhilash(9743688426)</li>
	            <li>In case of any disputes with registrations, contact the above mentioned numbers.</li>
	        </ul>
	</div>
	<br>
	<table class="table table-responsive table-hover">
	<tr>
		<th>Sl. No</th>
		<th>Participant Name</th>
		<th>Phone Number</th>
		<th>College</th>
		<th>Unique Code</th>
		<th>Tickmark</th>
	</tr>
	<?php
		include("../db_connect.php");
		if(!array_key_exists('name', $_GET)){
				header('Location:/');
		}
		$data = $_GET['name'];
		$data = htmlentities($data);
		$data = mysqli_real_escape_string($conn,$data);
		$event_name = $data;
		$member_name=$_SESSION['usrname'];
		$query = "SELECT * FROM participants WHERE event='$event_name'";
		$q = mysqli_query($conn,$query);
		$SlNo = 1;
		while($row = mysqli_fetch_assoc($q)){
			$id = $row['id'];
			$Name = $row['name'];
			$PhNo = $row['phno'];
			$College = $row['college'];
			$UniqueId = $row['unique_id'];
			echo "<tr><td>".$SlNo."</td><td>".$Name."</td><td>".$PhNo."</td><td>".$College."</td><td>".$UniqueId."</td><td></td></tr>";
			$SlNo = $SlNo + 1;
		}
		echo "</table><br><br><br>";
                echo "<div class='row'>";
                        echo "<div class='col-xs-4'>Winner</div>";
                        echo "<div class='col-xs-4'>Runner-up</div>";
                        echo "<div class='col-xs-4'>Event head signature</div>";
                echo "</div>";
                echo "<br><br><br>";
		$q2 = mysqli_query($conn,"SELECT p.timestamp from participants p ,events e where event='$event_name' order by p.timestamp desc ");
		$row = mysqli_fetch_assoc($q2);
		$Updated_time = $row['timestamp'];
		echo "</table>Report Generated at : ".$Updated_time;
//		echo "<br>";
//		echo "</table>Report Generated at : ".date('Y-m-d H:i:s',time()+19800);
?>
</table>
<hr>
<script>
    window.print();
</script>
</body>
</html>
