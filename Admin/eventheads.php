<html>
<head>
	<title>Event heads</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/style.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;lang=en" />
    <link href="/style.css" type="text/css" rel="stylesheet">
</head>
<body>
	<?php
	error_reporting(0);
	session_start();
	if(!isset($_SESSION['usrname']) || !isset($_SESSION['super']) || $_SESSION['super']!=1)
	{
		header('Location:/');
	}
	?>
	<?php
	include("../db_connect.php");
	?>
	<center>
	<div class='row'>
	<div class='col-sm-8 col-sm-offset-2'>
	<br><br><h1><strong>Event heads</strong></h1>
	<?php
		$query = "Select * from eventhead";
		$q = mysqli_query($conn,$query);
		echo "
			<table class=\"table table-responsive\">
				<tr>
					<th>Sl no.</th>
					<th>Event Name</th>
					<th>Student Name</th>
					<th>Phone Number</th>
				</tr>
		";
			$count = 1;
			while($row = mysqli_fetch_assoc($q))
			{
				$event_id = $row['event_id'];
				$event_name_query = "SELECT event_name from events where event_id = '$event_id'";
				$event_name_q = mysqli_query($conn,$event_name_query);
				$event_name_row = mysqli_fetch_assoc($event_name_q);	
				echo "
				<tr>
					<td>".$count."</td>
					<td>".$event_name_row['event_name']."</td>
					<td>".$row['name']."</td>
					<td>".$row['phno']."</td>
				</tr>";
				$count = $count + 1;
			}
		echo "
		</table>
		</div>
		</div>
		</center>
		";
		?>
		</body>
		</html>

