<html>
<head>
<title>
	Utsaha - Add Member
</title>
<?php include("../../bootstrap_header.html");  ?>
</head>
<body>
<?php
    session_start();
    if(!isset($_SESSION['usrname']) || !isset($_SESSION['super']) || $_SESSION['super']!=1)
    {
    	header('Location:/');
    }
    // define variables and set to empty values
    $nameerr = $usrerr = $usnerr = $phnoerr = $passworderr = $rpasserr = "";
    $name = $usrname = $phno = $password = $rpass = "";
    $err = 0;
    $errs=[];
    $msg=$errors="";
    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["name"])) {
            $nameerr = "Name is required";
		$errs[]=$nameerr;
            $err=1;
        } else {
            $name = test_input($_POST["name"]);
            if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
                $err=1;
                $nameerr = "Only alphabets and spaces are allowed";
		$errs[]=$nameerr;
            }
        }
        if (empty($_POST["phno"])) {
            $phnoerr = "Phone Number is required";
		$errs[]=$phnoerr;
            $err=1;
        } else {
            $phno = test_input($_POST["phno"]);
            if (!preg_match("/[0-9]{10}/",$phno)) {
                $err=1;
                $phnoerr = "Enter 10 digit mobile number";
		$errs[]=$phnoerr;
            }
        }
	if($err == 0)
	{
            include("../../db_connect.php");
            if(isset($_POST['name']) && isset($_POST['phno']))
            {
            	$phno = $_POST['phno'];
            	$name = $_POST['name'];
            	$query1 = "select * from members where name='$name' and phno='$phno'";
            	$q1 = mysqli_query($conn,$query1);
            	$count = mysqli_num_rows($q1);
            	if($count == 0)
            	{
                	$query = "Insert into members(name,password,phno) values('$name','$phno','$phno')";
                	$q = mysqli_query($conn,$query);
			$msg="Member Registered!";
            	}
            	else
            	{
            	   $errors="Account with the details already exists";
		   $errs[]=$errors;
            	}
            }
        }
}
    include("../../navbar.php");
?>
        <div class="container-fliud" id="wrapper">
            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-6"  id="main">
                <h1>Add Member</h1>
	<form name="register" action="/Admin/AddMember/" method="post">
		<input type="text" name="name" id="field" placeholder="Full Name (compulsory)"><br>
		<input type="tel" name="phno" id="field" placeholder="Phone Number"><br>
		<input type="submit" value="Register!" id="button">
	</form>
<?php
                                foreach($errs as $fieldError)
                                        echo "
                                 <div class=\"alert alert-danger\">
                                <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
                                " . $fieldError ."</div>";
                        if($msg != "")
                        {
                                echo "<div class=\"alert alert-success\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>".$msg."</div>";
                                $msg="";
                        }
                        ?>


    </div>
    <div class="col-md-3">
    <div>


</body>
</html>
