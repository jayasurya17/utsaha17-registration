<html>
<head>
<title>
	Utsaha - Add Event
</title>
<?php include("../../bootstrap_header.html");  ?>
</head>
<body>
<?php
		include("../../db_connect.php");
    error_reporting(E_ALL);
    session_start();
    if(!isset($_SESSION['usrname']) || !isset($_SESSION['super']) || $_SESSION['super'] != 1)
    {
        header('Location:/');
    }
      // define variables and set to empty values
    $nameerr = $numerr = $feeerr ="";
    $name = $usrname = $phno = $password = $rpass = "";
    $err = 0;
    $msg=$errors="";
    $errs=[];
    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["name"])) {
            $nameerr = "Name is required";
            $err=1;
	    $errs[]=$nameerr;
        } else {
            $name = test_input($_POST["name"]);
            if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
                $err=1;
                $nameerr = "Only alphabets and spaces are allowed";
	    $errs[]=$nameerr;
            }
        }

        if (empty($_POST["Number"])) {
            $numerr = "Max Number is required";
            $err=1;
	    $errs[]=$numerr;
        } else {
            $num = test_input($_POST["Number"]);
            if (!preg_match("/^[0-9]*$/",$num)) {
                $err=1;
                $numerr = "Only number is allowed";
            $errs[]=$numerr;
            }
        }

        if (empty($_POST["fee"])) {
          $feeerr = "Fee is required";
          $err=1;
	    $errs[]=$feeerr;
        }else {
            $fee = test_input($_POST["fee"]);
            if (!preg_match("/^[0-9]*$/",$fee)) {
                $err=1;
                $feeerr = "Enter a correct fee amount";
            $errs[]=$feeerr;
            }
        }
    }
    if($err == 0)
    {
        if(isset($_POST['name']) && isset($_POST['Number']) && isset($_POST['fee']))
        {
            $name = $_POST['name'];
          	$Number = $_POST['Number'];
          	$fee = $_POST['fee'];
          	$query1 = "select * from events where event_name='$name' and max_registrations='$Number' and fee='$fee'";
          	$q1 = mysqli_query($conn,$query1);
          	$count = mysqli_num_rows($q1);
          	if($count == 0)
          	{
            	$query = "Insert into events(event_name,max_registrations,fee) values('$name','$Number','$fee')";
            	$q = mysqli_query($conn,$query);
		$msg = "Event registered!";
          	}
          	else
          	{
                $errors="Event already exists";
		$errs[]=$errors;
          	}
        }
    }
    include("../../navbar.php");
?>
        <div class="container-fliud" id="wrapper">
            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-6"  id="main">
                <h1>Add Event</h1>
<form name="register" action="/Admin/AddEvent/" method="post">
    <input type="text" name="name" id="field" placeholder="Event Name"><br>
    <input type="text" name="Number" id="field" placeholder="Max Number Of Registrations"><br>
    <input type="text" name="fee" id="field" placeholder="Fee"><br>
    <input type="submit" value="Register!" id="button">
  </form>
<?php
                                foreach($errs as $fieldError)
                                        echo "
                                 <div class=\"alert alert-danger\">
                                <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
                                " . $fieldError ."</div>";
                        if($msg != "")
                        {
                                echo "<div class=\"alert alert-success\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>".$msg."</div>";
                                $msg="";
                        }
                        ?>
    </div>
    <div class="col-md-3">
    <div>
</body>
</html>
