<!doctype html>
<head>
	<title>Utsaha - Rules</title>
	<?php session_start(); ?>
	<?php include('../bootstrap_header.html'); ?>
	<?php if(isset($_SESSION['usrname'])){include('../navbar.php');} ?>
	<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.0.min.js"></script>
<style>
#myBtn {
  display: none;
  position: fixed;
  bottom: 20px;
  right: 30px;
  z-index: 99;
  border: none;
  outline: none;
  background-color: #ff6600;
  color: white;
  cursor: pointer;
  padding: 15px;
  border-radius: 10px;
}

#myBtn:hover {
  background-color: #E0E0E0;
}
</style>
</head>
<body>

	<div class="container-fluid" id="wrapper" style="text-align: left;">
<center>
<br>
<input list="all_events" name="events" id="events">
<datalist id="all_events">
    <option value="Fashion Show" name="fashion_show" >
    <option value="Solo Singing" name="solo_singing">
    <option value="Capture" name="capture">
    <option value="Group dance" name="group_dance">
    <option value="Solo Dance" name="solo_dance">
    <option value="Star Quest" name="star_quest">
    <option value="Coding" name="coding">
    <option value="Google It" name="google_it">
    <option value="Typathon" name="typathon">
    <option value="Mock Naukri" name="mock_naukri">
    <option value="Circuit Debugging" name="circuit_debugging">
    <option value="F UTSAHA L (FOOTBALL)" name="football">
    <option value="Basketball" name="basketball">
    <option value="Volleyball" name="volleyball">
    <option value="Gully cricket" name="gully_cricket">
    <option value="Ping Pong" name="ping_pong">
    <option value="Arrow (Darts)" name="arrow">
    <option value="Tug Of War" name="tug_of_war">
    <option value="EX’QUIZ’ME (GENERAL QUIZ)" name="general_quiz">
    <option value="HOGATHON" name="hogathon">
    <option value="BEG BORROW STEAL" name="beg_borrow_steal">
    <option value="BALLOONE'D" name="ballooned">
    <option value="DUMB AND DUMBER (DUMBCHARADES)" name="dumbcharades">
    <option value="GONE IN 60 SECONDS (Minute to win it)" name="minute_to_win_it">
    <option value="Sherlocked" name="sherlocked">
    <option value="FRIENDS and GOT quiz" name="f_g_quiz">
    <option value="Taboo" name="taboo">
    <option value="MEME DESIGN" name="meme_design">
    <option value="Comic Quest" name="comic_quest">
    <option value="COUNTER STRIKE 1.6 (5 on 5)" name="cs">
    <option value="FIFA 17" name="fifa">
    <option value="DOTA" name="dota">
    <option value="MINI MILITIA (3 on 3)" name="mini_militia">
    <option value="PSYCH" name="psych">
    <option value="Dr. RUBIX" name="rubix">
    <option value="ROAR" name="roar">
</datalist>
<button id="search">Search</button>
<br><br><a href="Utsaha_2017_Event_Rules.pdf" download>Download complete rules pdf</a>
<br><a href="Events_list.pdf" download>Download event list</a>
<br><a href="Utsaha_event_schedule.pdf" download>Download event schedule</a>
</center>

<center><h1>CULTURAL EVENTS</h1></center>

<div id="fashion_show">
    <h3>Fashion Show</h3>
	<ul>
	<li>No restrictions with the theme (Open themed)</li>
	<li>Each team shall be alloted 8+2 minutes</li>
	<li>A maximum of 18 participants are allowed per team.</li>
	<li>The final music needs to be submitted in CD/pen drive to the sound department.</li>
	<li>Please report to the college premises by 1PM</li>
	<li>All Participants must carry their college ID cards and other essentials.</li>
	<li>The slots for the performance will be assigned on the basis of chits pocked prior to the event.</li>
	<li>Any team which would like to control the sound and light department would have to inform the coordinator a day before the event.</li>
	</ul>
</div>

<div id="solo_singing">
    <h3>Jigglypuff (Solo singing)</h3>
    <ul>
        <li>Time limit: 3+1 minutes</li>
        <li>A singer is allowed at most two instruments accompanying OR
                       a karaoke.</li>
        <li>Accompanying instrument can be strings or a key board. No
                        percussi</li>
        <li>Karaoke track should be handed on a pendrive over to the event
                    coordinators before the start of the event</li>
    </ul>
</div>

<div id="capture">
    <h3>CAPTURE</h3>
    <ul>
        <li>Do nothing, just shoot.</li>
    </ul>
</div>

<div id="group_dance">
    <h3> Group dance</h3>
    <ul>
        <li>Team can consist of a maximum of 15 members, minimum 6 (atleast 5
        people on stage at any given time)</li>
        <li>Group event starts with a choreographed (5+2) mins sequence.
        Any team not ready at the start of the event will stand disqualified.
        The Judge’s decision is final and binding.</li>
        <li>If props are being used, organisers should be informed before hand.
        The organizing team holds the right to change the rules at any time
        Fire,water and hazardous objects are NOT allowed on stage.</li>
        <li>Teams must submit their audio 60 minutes prior to the event in a CD or
          pen drive in MP3 format.</li>
    </ul>
</div>

<div id="solo_dance">
    <h3>Break-a-leg (SOLO DANCE)</h3>
    <ul>
        <li>The time limit is 3+1 minutes.</li>
        <li>Teams are instructed to submit their audio tracks in a MP3 format in a cd or
                     pen drive 2 hours prior to the event.</li>
        <li>Please ensure your music is not in a phone.</li>
        <li>Dangerous Props Not Allowed.</li>
        <li>Judges decision is final.</li>
        <li>Contact the organisers as soon as you reach the venue.</li>
    </ul>
</div>

<div id="star_quest">
    <h3>STAR QUEST</h3>
    <ul>
        <li>Teams/participants has to show their talent in any field(like singing, dancing, comedy, art etc.)</li>
        <li>Judges decision will be final.</li>
    </ul>
</div>


<center><h1>TECHNICAL EVENTS</h1></center>

<div id="coding">
    <h3>Coding</h3>
    <ul>
        <li>All rounds will be coding challenges which has to be solved.</li>
        <li>Participants can bring their own laptops.</li>
        <li>The following run-times/compilers will be provided on the lab computers - C, C++,
        Java, Python2, Python3, Ruby, JavaScript (NodeJS), PHP 5.5.</li>
        <li> If there are more than 40 participants there might be an elimination round at the
        beginning  consisting MCQ’s in C,C++</li>
    </ul>
</div>


<div id="google_it">
    <h3>Google It</h3>
    <ul>
        <li>Maximum of 2 participants in a group.</li>
        <li>Internet access will be provided.</li>
        <li>The team that completes maximum puzzles with correct answer wins.</li>
        <li>If there is a tie, timer is taken into consideration.</li>
    </ul>
</div>

<div id="typathon">
    <h3>Typathon</h3>
    <ul>
        <li>1st  place:1000/-</li>
        <li>Typathon consists of 3 rounds.</li>
        <li>Each round is of 1 minute.</li>
        <li>The person with most score at the end of all rounds will be declared as the winner.</li>
    </ul>
</div>

<div id="mock_naukri">
    <h3>Mock Naukri</h3>
    <ul>
        <li>1st  place:2500/- 2nd place:1500/-</li>
        <li>There will be three rounds:</li>
        <li>Aptitude test:  It will be an objective test consisting of both technical and
        quantitative questions. The test will be of 1 hour having 40 questions. No extra time
        will be given. Each right answer will be awarded 3 marks. There will be negative
        marking for each wrong answer.</li>
        <li>Group discussion round: Participants will be divided into small groups. Each group
        will be given a topic and five minutes will be given for discussion.</li>
        <li>HR Round: This will be the final round with one on one interview with the HR.</li>
    </ul>
</div>

<div id="circuit_debugging">
    <h3>CIRCUIT DEBUGGING</h3>
    <ul>
        <li>Standard Rules</li>
    </ul>
</div>


<div id="football">
    <h3> F UTSAHA L (FOOTBALL)</h3>
    <ul>
        <li>Two halves of 10 minutes each. If there is no result at the end of two halves, we
head for a shootout-3 kicks to be taken by each side. If there is no result we head
for a sudden death.</li>
        <li>Rules for penalty kick-A box will be made and the striker has to shoot from within
        the box. Any one foot  should be grounded and the shooter cannot take a layup or
        run and hit the ball.In case of backpass free kick will be given outside the D.</li>
        <li>Once the ball goes out-of- bounds  the team will have to kick-in the ball rather
        than throw-in .Ball cannot be directed  towards the goal post. The first pass is
        free.</li>
        <li>Teams will comprise of 6 players including goalkeeper and 2 rolling substitutes.
        A break of 2 minutes will be given between two halves.</li>
    </ul>
</div>

<div id="basketball">
    <h3>BASKETBALL</h3>
    <ul>
        <li>The maximum number of players allowed will be 5(3 starters+2 substitutes)</li>
        <li>Player who scores within the 3 point arc will be awarded 2 points and outside
        the 3 pointer line will be awarded 3 points.</li>
        <li>Each team is allowed a single time out in each half for a minute .
        After half time there will be 1min break.</li>
        <li>POSSESSION SYSTEM-for any change in ball possession between the two teams
        the opposing team should clear the 3 point arc (LOSER’S BALL-which means if a
        team scores the opponents gets the ball).</li>
        <li>If there is a tie at the end then 5 mins will be given to decide the winner</li>
    </ul>
</div>

<div id="volleyball">
    <h3>VOLLEYBALL</h3>
    <ul>
        <li>Standard volley ball rules.</li>
    </ul>
</div>

<div id="gully_cricket">
    <h3>Gully Cricket</h3>
    <ul>
        <li>20 balls per team . Each team comprises of 6+1 players.</li>
        <li>One over consists of 4 balls and only one bowler can bowl two overs.</li>
        <li>Sixer is allowed below a 5-feet rope.</li>
        <li>Over the rope directly is given out.</li>
        <li>One player can play only for one team.</li>
        <li>Umpire decision is final</li>
        <li>Any argument with the umpire or any of the officials will lead to immediate disqualification</li>
    </ul>
</div>

<div id="ping_pong">
    <h3>Ping Pong (Table Tennis Men/Women Singles)</h3>
    <ul>
        <li>It is a best of 3 sets.</li>
        <li>For final matches it is best of 5 sets.</li>
        <li>All matches will be conducted as per standard Table Tennis rules.</li>
        <li>Referee’s decision is final.</li>
        <li>Any argument with the umpire or any of the officials will lead to immediate disqualification</li>
    </ul>
</div>

<div id="arrow">
    <h3>ARROW (DARTS)</h3>
    <ul>
        <li>Individual participation.</li>
        <li>Group of 5 random participants will be made.</li>
        <li>Each player will get 5 throws.</li>
        <li>Top 2 from a group will qualify for next rounds.</li>
        <li>Coordinator decision is final</li>
    </ul>
</div>

<div id="tug_of_war">
    <h3>TUG OF WAR</h3>
    <ul>
        <li>Standard rules.</li>
    </ul>
</div>

<center><h1>LITERARY EVENT<center></h1></center>

<div id="general_quiz">
    <h3>EX’QUIZ’ME (GENERAL QUIZ)</h3>
    <ul>
        <li>There shall be 2 members per team</li>
        <li>Quiz masters decision will be final</li>
    </ul>
</div>

<center><h1>FUN EVENTS<center></h1></center>

<div id="hogathon">
    <h3>Death by Food</h3>
    <ul>
        <li>Individual Participation</li>
        <li>Standard Rules apply.</li>
        <li>Time limit will be informed.</li>
    </ul>
</div>

<div id="beg_borrow_steal">
    <h3>BEG BORROW STEAL</h3>
    <ul>
        <li>Time limit will be informed.</li>
        <li>Final decision to be taken by the panel</li>
        <li>3 per team</li>
        <li>Further details to be delivered on spot</li>
    </ul>
</div>

<div id="ballooned">
    <h3>BALLOONE’D</h3>
    <ul>
        <li>Time limit will be informed.</li>
        <li>4 per team</li>
        <li>Further details to be delivered on spot</li>
    </ul>
</div>

<div id="dumbcharades">
    <h3>DUMB AND DUMBER (DUMBCHARADES)</h3>
    <ul>
        <li>3 members per team</li>
        <li>4 rounds</li>
        <li>No malpractice</li>
        <li>Coordinator decision is final</li>
        <li>No gadgets allowed</li>
    </ul>
</div>

<div id="minute_to_win_it">
    <h3>GONE IN 60 SECONDS (Minute to win it)</h3>
    <ul>
        <li>1st  place:2000/- 2nd place:1000/-</li>
        <li>Team of two.</li>
        <li>Teams will be given a task which they have to finish in 1 minute.</li>
        <li>Multiple rounds.</li>
        <li>Every round is an elimination round.</li>
        <li>Coordinator’s decision is final.</li>
    </ul>
</div>


<div id="sherlocked">
    <h3>Sherlocked</h3>
    <ul>
        <li>Round 1: <br>
Elimination <br>
Teams will be eliminated through a general quiz consisting of questions based on Crime Fiction Tv shows, Cartoons (famous crime/detective related), Riddles and puzzles. <br>

20-25 questions will be given as a written test. Top 20 teams based on highest scores move on to the next round.
</li>
    <li>Round 2:  <br>
Solving the Mystery  <br>

Teams have to investigate a crime scene and find the culprit with proper justification of the crime.
</li>
    </ul>
</div>

<div id="f_g_quiz">
    <h3>FRIENDS and GOT quiz</h3>
    <ul>
        <li>Maximum 2 members Per Team.</li>
        <li>Rounds (Standard Quiz rules).</li>
        <li>Mixed questions from both GoT and F.R.I.E.N.D.S will be asked.</li>
    </ul>
</div>

<div id="taboo">
    <h3>TABOO</h3>
    <ul>
        <li>2 points for all correct guesses. </li>
        <li>-1 point for each taboo word said.</li>
        <li>One person will be the guesser and one clue-giver. Clue-giver has to make his partner guess the word given to him, without using any taboo words.</li>
        <li>5 taboo words for each word to be guessed.</li>
        <li>No gestures allowed.</li>
        <li>No use of sentences or phrases.</li>
    </ul>
</div>

<div id="meme_design">
    <h3>MEME DESIGN</h3>
    <ul>
        <li>Event type: Individual</li>
        <li>Challenge: Design the best meme you can on a current affairs topic we set on the day of event.</li>
        <li>Duration: 2 days of Utsaha 2017 to finalize your design.</li>
        <li>Provisions: Basic online image editor + Sample Images + Custom Image uploader.</li>
        <li>Judgement: A panel of evaluators will decide winners through collective vote.</li>
        <li>Winners: Receive goodies, cash prize, publicity in official site and FB page.</li>
        <li>Devices: You may use either personal laptops or desktops provided in BMSIT&M.</li>
        <li>Other: If the winners aren’t available on the second day of event, we will mail goodies/prizes to their residential address.</li>
    </ul>
</div>

<div id="comic_quest">
    <h3>Comic Quest</h3>
    <ul>
        <li>Team of 3.</li>
        <li>All other rules will be notified on the spot.</li>
        <li>No malpractice.</li>
    </ul>
</div>

<center><h1>GAMING</h1></center>

<div id="cs">
    <h3>COUNTER STRIKE 1.6 (5 on 5)</h3>
    <ul>
        <li>5 in a Team.</li>
        <li>Participants can bring their own gears like mouse, headphones etc.</li>
        <li>Maps: 1000$, 2000$ and fy maps for initial rounds.</li>
        <li>Bombsites for last three rounds</li>
    </ul>
</div>

<div id="fifa">
    <h3>FIFA 17</h3>
    <ul>
        <li>All games will be on PS4</li>
    </ul>
</div>

<div id="dota">
    <h3>DOTA</h3>
    <ul>
        <li>Contact: 7795506800 , Shiv Mangal Singh</li>
    </ul>
</div>

<div id="mini_militia">
    <h3>MINI MILITIA (3 on 3)</h3>
    <ul>
        <li>A team will be having 3 members.</li>
        <li> Gameplay: 3 on 3 (quick knockout)</li>
        <li> No hacked or pro version of the game is allowed.</li>
        <li>The team which is not appearing at the venue in time is disqualified.</li>
        <li>Doodle Army 2: Mini Militia apk is provided at the time of game play</li>
        <li>In case of lagging issues, final decision will be taken by the coordinat</li>
    </ul>
</div>

<div id="psych">
    <h3>PSYCH</h3>
    <ul>
        <li>Round 1:<br> 4 random participants will be connected on a server.<br>Top 2 players from each game will qualify for the next round.</li>
        <li>Subsequent rounds: Same as Round 1. In last round, the player with highest score wins.</li>
    </ul>
</div>

<div id="rubix">
    <h3>Dr. RUBIX</h3>
    <ul>
        <li>Two players compete with each other. Maximum time allotted per battle will be 5 minutes. Total 3 battles. The player who wins maximum (2) battles will win.</li>
        <li>Coordinators decision is final.</li>
    </ul>
</div>

<center><h1>OTHERS</h1></center>
<div id="roar">
    <h3>ROAR</h3>
    <ul>
        <li>Categories: <br>Below 300 cc<br>300 – 500 cc<br>Above 500 cc</li>
        <li>Bike should be mounted on its centre stand</li>
        <li>The participants bike should have a silencer and the complete exhaust system</li>
        <li>Only geared bikes allowed</li>
        <li>Registration message is mandatory and has to be carried on the day of event</li>
        <li>The bike can participate only in the categories they belong</li>
        <li>Only one trail shall be given to every biker and a maximum of 10s to Rev the bike</li>
        <li>No removal of baffle plates or other mandatory parts of an exhaust system</li>
        <li>No second chance shall be given at any cost</li>
        <li>No turning 'OFF'  or turning  'ON' the kill switch while revving.</li>
        <li>The decision of the event organizer is final</li>
    </ul>
</div>

<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>

	<div>	
</body>

<script>
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
</script>

<script>
    $("#search").click(function() {
        var val = $('#events').val();
        var name = $('#all_events option[value="'+val+'"]').attr('name');
        $('html, body').animate({
            scrollTop: $("#"+name).offset().top - 10
        }, 500);
    });
</script>

</html>
