<doctype! html>
<head>
<?php include("../bootstrap_header.html"); ?>
<title>Utsaha - Leaderboard</title>
</head>
<body>
<?php
	session_start();
	if(!isset($_SESSION['usrname']))
	{
		header('Location:/');
	}			
	include("../navbar.php");
?>
		<div class="container-fluid" id="wrapper">
			<h1>Leaderboard</h1>
			<hr>
	        <div class="row">
	            <div class="col-md-3">
	            </div>
	            <div class="col-md-6" id="main">
	            <!--add backend here 	                                    -->
				   	<div id="amount_collected">
	            		<strong>Amount Collected:
            				<?php
            					include("../db_connect.php");
            					$q=mysqli_query($conn,"SELECT sum(e.fee) as sum from events e, participants p where p.event=e.event_name");
            					$row=mysqli_fetch_assoc($q);
            					echo $row['sum'];
            					?>

	            		</strong>
	         		<hr>
	            	</div>
					<table class="table table-responsive table-hover">
					  	<tr>
					  		<th id="rank">Rank</th>
					    	<th id="name">Name</th>
					    	<th id="total_amount">Total Amount</th>
					  	</tr>
					  	<?php
							include("../db_connect.php");
							$mainquery="Select name from members";
							$mainq=mysqli_query($conn,$mainquery);
							while($row = mysqli_fetch_assoc($mainq))
							{
								$member_name=$row['name'];
								$query = "Select e.fee from events e,participants p where e.event_name = p.event and p.registrar_name='$member_name'";
								$q = mysqli_query($conn,$query);
								$amount=0;
								while($row1 = mysqli_fetch_assoc($q))
									$amount += $row1['fee'];
								$leaders[$member_name]=$amount;		
							}
							arsort($leaders);
							$rank = 1;
							foreach($leaders as $x => $x_value) {
								echo "<tr><td>".$rank."</td><td>";
									if(isset($_SESSION['super']) && $_SESSION['super']==1)
									{
										echo "<a href='/Admin/member_details.php?name=".$x."' target='_blank'>".$x."</a>";
									}
									else
									{
										echo $x;
									}
									echo "</td><td>".$x_value."</td></tr>";
									$rank = $rank + 1;
							}
						?>
					</table>
	            </div>
	        <div class="col-md-3">
			</div>
	    </div>
	</div>
        </div>
       </boby>
       </html>
