<html>
<head>
<title>
	Utsaha - Change Password
</title>
<?php include("../bootstrap_header.html"); ?>
</head>
<body>
    <?php
        session_start();
        if(!isset($_SESSION['usrname']))
        {
            header('Location:/');
        }
        include("../navbar.php");
        if(!isset($_SESSION['usrname']))
        {
        	header('Location:/');
        }
        function test_input($data) {
          $data = trim($data);
          $data = stripslashes($data);
          $data = htmlspecialchars($data);
          return $data;
        }
	$errs=[];
        $err=0;
        $msg=$errors="";
        $cpasserr=$npasserr=$rnpasserr="";
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
          if (empty($_POST["current_password"])) {
            $cpasserr = "Field is required";
            $err=1;
	    $errs[]=$cpasserr;
          } else {
            $cpass = test_input($_POST["current_password"]);
            if (!preg_match("/(.*){8,}/",$cpass)) {
             $err=1;
              $cpasserr = "Enter atleast 8 characters as password";
		$errs[]=$cpasserr;
            }
          }
          if (empty($_POST["new_password"])) {
            $npasserr = "Enter a new password";
	    $errs[]=$npasserr;
            $err=1;
          } else {
            $npass = test_input($_POST["new_password"]);
            if (!preg_match("/(.){8,}/",$npass)) {
             $err=1;
              $npasserr = "Enter atleast 8 characters as password";
	    $errs[]=$npasserr;
            }
          }
          if (empty($_POST["re_password"])) {
            $rnpasserr = "Confirm password";
	    $errs[]=$rnpasserr;
            $err=1;
          } else {
            $rpass = test_input($_POST["re_password"]);
            if (!preg_match("/(.){8,}/",$rpass)) {
             $err=1;
              $rnpasserr = "Enter atleast 8 characters as password";
	    $errs[]=$rnpasserr;
            }
          }
          if ($err == 0)
          {
        	if($npass != $rpass)
        	{
        		$rnpasserr="Passwords do not match";
			$errs[]=$rnpasserr;
        		$err=1;
        	}
          }
          if($err == 0)
          {
	include("../db_connect.php");
	$usrname=$_SESSION['usrname'];
	$query="Select * from members where name='$usrname' and password='$cpass'";
	$q=mysqli_query($conn,$query);
	$row = mysqli_fetch_assoc($q);
	$count = mysqli_num_rows($q);
	if($count == 1)
	{
		$query1 = "update members set password='$npass' where name='$usrname'";
		$q1 = mysqli_query($conn,$query1);
	 $msg="Password Changed Successfully!";
	}
	else
	{
		$errors="Incorrect Password";
		$errs[]=$errors;
	}
  }
  }
?>
<div class="container-fliud" id="wrapper">
    <div class="row">
	<div class="col-md-3">
	</div>
	<div class="col-md-6"  id="main">
<h3>Change your password!</h3>
<form action='/ChangePassword/' method='post'>
<input type="password" name="current_password" id="field" placeholder="Current Password"><br><br>
<input type="password" name="new_password" id="field" placeholder="New Password"><br><br>
<input type="password" name="re_password" id="field" placeholder="Confirm Password"><br><br>
<input type="submit" value="Change" id="button">
</form>
<?php
                                foreach($errs as $fieldError)
                                        echo "
                <div class=\"alert alert-danger\">
                        <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
                        " . $fieldError ."</div>";
		if($msg!="")
			echo "
                <div class=\"alert alert-success\">
                        " . $msg ."<strong>  You will be logged out in 5s. Please Login to continue</strong></div>";

                        ?>


</div>
<?php
	if($msg!="")
		header("refresh:5;url=/logout.php");
?>

                <div class="col-md-3">
                </div>
                </div>
</body>
</html>
