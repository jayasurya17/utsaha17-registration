<doctype! html>
<head>
<?php include("../bootstrap_header.html"); ?>
<title>Utsaha - Event Details</title>

</head>
<body>
	<?php
		session_start();
		if(!isset($_SESSION['usrname']))
		{
			header('Location:/');
		}			
		include("../navbar.php");
	?>

	<div class="container-fluid" id="wrapper">

		<h1>Event Details</h1><hr>
	        <div class="row">
	            <div class="col-md-2">
	            </div>
	            <div class="col-md-8 col-sm-12"  id="main">
	            <!--add backend here 	                                    -->
				   	<div id="amount_collected">

	            		<strong>Amount Collected:
            				<?php
            					include("../db_connect.php");
            					$q=mysqli_query($conn,"SELECT sum(e.fee) as sum from events e, participants p where p.event=e.event_name");
            					$row=mysqli_fetch_assoc($q);
            					echo $row['sum'];
            					?>
	            		</strong>
	            	</div>
	            <hr>

			<table class="table table-responsive">
			<tr>
				  	<th>Sl no.</th>
				  	<th>Event Name</th>
				  	<th>Number of registrations</th>
				  	<th>Max Registrations</th>
				  	<th>Fee</th>
				  	<th>Total Amount</th>
				  </tr>
				  <?php
						include("../db_connect.php");
						$member_name=$_SESSION['usrname'];
						$query = "select * from events";
						$q = mysqli_query($conn,$query);
						$slno = 1;
						while($row = mysqli_fetch_assoc($q)){
							$event_name = $row['event_name'];
							$number_of_registrations = $row['no_of_registrations'];
							$max_registrations = $row['max_registrations'];
							$fee = $row['fee'];
							$total = $number_of_registrations * $fee;
							echo "<tr><td>".$slno."</td><td>";
							if(isset($_SESSION['super']) && $_SESSION['super']==1)
							{
								echo "<a href='/Admin/event_details.php?name=".$event_name."'>".$event_name."</a>";
							}
							else
							{
								echo $event_name;
							}
							echo "</td><td>".$number_of_registrations."</td><td>".$max_registrations."</td><td>".$fee."</td><td>".$total."</td></tr>";
							$slno = $slno + 1;
						}
					?>
			</table>
	            </div>
	        <div class="col-md-2">
			</div>
	</div>	
		
</body>
</html>
