<doctype! html>
<head>

	<title>Utsaha</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="/style.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="/bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;lang=en" />
	<link href="/style.css" type="text/css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
</head>
<?php
session_start();
if(isset($_SESSION['usrname']))
    {
            header('Location: /Home');

    }
    function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
// define variables and set to empty values
$nameerr = $usrerr = $phnoerr = $passworderr = $rpasserr = "";
$name = $usrname = $phno = $password = $rpass = "";
$err = 0;
$errors="";
$errs=[];

if ($_SERVER["REQUEST_METHOD"] == "POST") {

  if (empty($_POST["phno"])) {
    $phnoerr = "Phone Number is required";
    $errs[]=$phnoerr;
    $err=1;
  } else {
    $phno = test_input($_POST["phno"]);
    if (!preg_match("/^[0-9]{10}$/",$phno)) {
     $err=1;
      $phnoerr = "Enter 10 digit phone number";
    $errs[]=$phnoerr;
    }
  }


  if (empty($_POST["password"])) {
    $passworderr = "Password is required";
    $errs[]=$passworderr;
    $err=1;
  } else {
    $password = test_input($_POST["password"]);
    if (!preg_match("/.{8,}/",$password)) {
     $err=1;
      $passworderr = "Enter atleast 8 characters as password";
 $errs[]=$passworderr;
    }
  }






if($err == 0)
{
include("db_connect.php");

if(isset($_POST['phno']) && isset($_POST['password']))
{
    $phno = $_POST['phno'];
    $password = $_POST['password'];
    $query = "SELECT * from members where phno='$phno' and password='$password'";
    $q = mysqli_query($conn,$query);
    $row = mysqli_fetch_assoc($q);
    $login = $row['Login'];
    $count = mysqli_num_rows($q);
    if($count == 1)
    {
        $_SESSION['usrname'] = $row['name'];
        $_SESSION['phno'] = $row['phno'];
        $_SESSION['super']=$row['super'];
	if($row['Login'] == 0)
	{
        	mysqli_query($conn,"UPDATE members set Login='1' where  phno='$phno' and password='$password'");
		header('Location:/ChangePassword');
	}
	else
        	header('Location:/Home');
    }
    else
    {
        $errors = "Member Doesnt Exists in db";
	$errs[]=$errors;
    }
}

}
}
?>
<body>

	<div class="container-fluid">

        <h1 id="semaphore">Utsaha 2017<strong style="color:#ff6600;">.</strong></h1>
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6"  id="main">
                <form name="register" action="/" method="post" style="padding-top:40px;">
                    <input type="tel" name="phno" id="phoneNo" placeholder="Phone Number"><br>
                    <input type="password" name="password" id="password" placeholder="Password"><br>
                    <input type="submit" value="Login" id="button">
                </form>
			<?php
				foreach($errs as $fieldError)
					echo "
		<div class=\"alert alert-danger\">
			<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
			" . $fieldError ."</div>";
			?>
            </div>
            <div class="col-md-3">
        </div>
    </div>

    <footer id="footer">
        <p>Powered by:</p>
       <a href="https://thefreesoftware.club"> <img src="Images/tfsc.svg" height="85px" width="300px" id="tfscLogo" align="center"></a>
    </footer>
</div>
</body>
</html>
