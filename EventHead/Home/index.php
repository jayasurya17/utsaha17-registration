<?php
 include("../../bootstrap_header.html");

    session_start();
    include("../../db_connect.php");
    $id = $_SESSION['eventheadid'];
    // echo $id;
    $query = "SELECT * from eventhead where slno='$id'";
    // echo $query;
    $q = mysqli_query($conn,$query);
    // echo $q;
    $count = mysqli_num_rows($q);
    if($count == 1)
    {
        $row = mysqli_fetch_assoc($q);
        $name = $row['name'];
        $event_id = $row['event_id'];
        $query = "SELECT * from events where event_id=$event_id";
        $q = mysqli_query($conn,$query);
        $row = mysqli_fetch_assoc($q);
        $no_of_registrations = $row['no_of_registrations'];
	$event_name = $row['event_name'];
        $max_registrations = $row['max_registrations'];
        if($no_of_registrations == $max_registrations){
            header('Location: /EventHead/participants_list.php');
        }else{
            echo "<center><h1>Welcome $name</h1><h1>Event : $event_name</h1>
            <h6>You must first stop registration to download participants list. Once stopped it cannot be restarted.<strong> Use with caution.</strong></h6>
	    <h6>If you are unable to download or take print out after you stop registration contact Abhilash(9743688428) or Jayasurya(8197360630).</h6>
           <button class='btn btn-danger' onclick='func();'>Stop registration</button>";
	        echo '<table class="table table-responsive table-hover">
	        <tr>
	        <th>Sl. No</th>
	        <th>Participant Name</th>
	        <th>Phone Number</th>
	        <th>College</th>
	        </tr>';
        	$query = "SELECT * FROM participants WHERE event='$event_name'";
	        $q = mysqli_query($conn,$query);
        	$SlNo = 1;
	        while($row = mysqli_fetch_assoc($q)){
	            $id = $row['id'];
        	    $Name = $row['name'];
	            $PhNo = $row['phno'];
        	    $College = $row['college'];
	            echo "<tr><td>".$SlNo."</td><td>".$Name."</td><td>".$PhNo."</td><td>".$College."</td></tr>";
	            $SlNo = $SlNo + 1;
	        }
		echo '</table>';
	}
    }
	else
    {
        header('Location:/EventHead/logout.php');
    }
?>
<script>
    function func(){
        var check=confirm("Are you sure ?");
        if(check==true){
            window.location ="../stop_registration.php";
        }
    }
</script>  
