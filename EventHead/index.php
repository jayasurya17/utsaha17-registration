<doctype! html>
<head>

	<title>Utsaha 2017</title>
  <?php include("../bootstrap_header.html");  ?>
</head>
<?php
    session_start();
    if(isset($_SESSION['eventheadid']))
    {
        header('Location: /EventHead/Home');

    }
    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    // define variables and set to empty values
    $nameerr = $usrerr = $phnoerr = $passworderr = $rpasserr = "";
    $name = $usrname = $phno = $password = $rpass = "";
    $err = 0;
    $errors="";
    $errs=[];

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        //echo "hello";
        if (empty($_POST["phno"])) 
        {
            $phnoerr = "Phone Number is required";
            $errs[]=$phnoerr;
            $err=1;
        } 
        else 
        {
            $phno = test_input($_POST["phno"]);
            if (!preg_match("/^[0-9]{10}$/",$phno)) 
            {
                $err=1;
                $phnoerr = "Enter 10 digit phone number";
                $errs[]=$phnoerr;
            }
        }
        if (empty($_POST["password"])) 
        {
            $passworderr = "Password is required";
            $errs[]=$passworderr;
            $err=1;
        }
        else 
        {
            $password = test_input($_POST["password"]);
            if (!preg_match("/.{8,}/",$password) || !preg_match("/^[0-9][0-9]a[0-9][0-9][0-9]m[0-9][0-9][0-9][0-9]$/",$password)) 
            {
                $err=1;
                $passworderr = "Invalid password";
                $errs[]=$passworderr;
            }
        }

        if($err == 0)
        {
            include("../db_connect.php");
            if(isset($_POST['phno']) && isset($_POST['password']))
            {
                
            // echo "test";
                $phno = $_POST['phno'];
                $password = $_POST['password'];
                $query = "SELECT * from eventhead where phno='$phno' and password='$password'";
                $q = mysqli_query($conn,$query);
                $row = mysqli_fetch_assoc($q);
                $count = mysqli_num_rows($q);
                if($count == 1)
                {
                    $_SESSION['eventheadid'] = $row['slno'];
                    $_SESSION['eventid'] = $row['event_id'];
                    // echo $_SESSION['eventheadid'];
                    header('Location: /EventHead/Home');
                }
                else
                {
                    $errors = "Member Doesnt Exists in db";
                    $errs[]=$errors;
                }
            }
        }
    }
?>
<body>

	<div class="container-fluid">

        <h1 id="semaphore">Event head login<strong style="color:#ff6600;">.</strong></h1>
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6"  id="main">
                <form name="register" action="/EventHead/" method="post" style="padding-top:40px;">
                    <input type="tel" name="phno" id="phoneNo" placeholder="Phone Number"><br>
                    <input type="password" name="password" id="password" placeholder="Password"><br>
                    <input type="submit" value="Login" id="button">
                </form>
			<?php
				foreach($errs as $fieldError)
					echo "
		<div class=\"alert alert-danger\">
			<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
			" . $fieldError ."</div>";
			?>
            </div>
            <div class="col-md-3">
        </div>
    </div>

    <footer id="footer">
        <p>Powered by:</p>
        <img src="../Images/tfsc.svg" height="85px" width="300px" id="tfscLogo" align="center">
    </footer>
</div>
</body>
</html>
