
<html>
	<head>
		<meta charset="utf-8">
	<title>Participants</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/style.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;lang=en" />
    <link href="/style.css" type="text/css" rel="stylesheet">
		</head>
		<body>
	<?php
	error_reporting(0);
	session_start();
    if(!isset($_SESSION['eventheadid']) || !strcmp($_SESSION['eventheadid'],""))
    {
    		header('Location:/EventHead/logout.php');
    }
	?>
	<?php
	include("../db_connect.php");
    $id = $_SESSION['eventheadid'];
    $event_id = (int)$_SESSION['eventid'];
    $query = "SELECT * from eventhead where slno='$id'";
    $q = mysqli_query($conn,$query);
    $row = mysqli_fetch_assoc($q);
    $count = mysqli_num_rows($q);
    if($count == 1)
    {
        $name = $row['name'];
    }
?>
<html>
<head>
<title>
	Utsaha 2017
</title>
<?php include("../bootstrap_header.html");?>
</head>
<body>
    <?php
        error_reporting(0);
        session_start();
        if(!isset($_SESSION['eventheadid']) || !strcmp($_SESSION['eventheadid'],""))
        {
        		header('Location:/EventHead/logout.php');
        }
        include("../db_connect.php");
        $id = $_SESSION['eventheadid'];
        $event_id = (int)$_SESSION['eventid'];
        $query = "SELECT * from events where event_id='$event_id'";
        $q = mysqli_query($conn,$query);
        $row = mysqli_fetch_assoc($q);
        $count = mysqli_num_rows($q);
        if($count == 1)
        {
            $event_name = $row['event_name'];
        }
    ?>
	<h1><?php echo $event_name ?></h1><hr>
	<div style="text-align:left">
	        <h3>Event heads please ensure that the following are done</h3>
       		<ul>
	            <li>Verify the access code or college ID card of participants before allowing them inside.</li>
	            <li>Mark a tick in each participant's row to indicate that they have participated.</li>
	            <li>Sign at the bottom of this report with your name.</li>
	            <li>Return these sheets to  Jayasurya(8197360630) or Abhilash(9743688426)</li>
	            <li>In case of any disputes with registrations, contact the above mentioned numbers</li>
	        </ul>
	</div>
	<br>
	<table class="table table-responsive table-hover">
	<tr>
		<th>Sl. No</th>
		<th>Participant Name</th>
		<th>Phone Number</th>
		<th>College</th>
		<th>Unique Code</th>
		<th>Tickmark</th>
	</tr>
	<?php
		include("../db_connect.php");
		if(!strcmp($event_name,"")){
				header('Location:/EventHead/logout.php');
		}
		$query = "SELECT * FROM participants WHERE event='$event_name'";
		$q = mysqli_query($conn,$query);
		$SlNo = 1;
		while($row = mysqli_fetch_assoc($q)){
			$id = $row['id'];
			$Name = $row['name'];
			$PhNo = $row['phno'];
			$College = $row['college'];
			$UniqueId = $row['unique_id'];
			echo "<tr><td>".$SlNo."</td><td>".$Name."</td><td>".$PhNo."</td><td>".$College."</td><td>".$UniqueId."</td><td></td></tr>";
			$SlNo = $SlNo + 1;
		}


		echo "</table><br><br><br>";
		echo "<div class='row'>";
			echo "<div class='col-xs-4'>Winner</div>";
			echo "<div class='col-xs-4'>Runner-up</div>";
			echo "<div class='col-xs-4'>Event head signature</div>";
		echo "</div>";
		echo "<br><br><br>";
		$q2 = mysqli_query($conn,"SELECT p.timestamp from participants p ,events e where event='$event_name' order by p.timestamp desc ");
		$row = mysqli_fetch_assoc($q2);
		$Updated_time = $row['timestamp'];
		echo "</table>Last Updated at : ".$Updated_time;
		echo "<br>";
		echo "</table>Report Generated at : ".date('Y-m-d H:i:s',time()+19800);
?>
</table>
<hr>
</body>
<script>
window.print();
</script>
</html>
