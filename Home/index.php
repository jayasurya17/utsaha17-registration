<html>
<head>
<title>
	Utsaha - Registration
</title>
<?php include('../bootstrap_header.html'); ?>
</head>
<body>
<?php
session_start();
if(!isset($_SESSION['usrname']) || !strcmp($_SESSION['usrname'],"") )
{

		header('Location:/logout.php');
}
include("../db_connect.php");
$errs=[];
$nameerr = $usrerr = $usnerr = $phnoerr = $passworderr = $rpasserr = "";
$name = $usrname = $phno = $password = $rpass = "";
$err = 0;
$errors=$msg ="";
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
if ($_SERVER["REQUEST_METHOD"] == "POST" && !isset($_POST['clear'])) {
	$_SESSION['msg']="";
  if (empty($_POST["name"])) {
    $nameerr = "Name is required";
    $errs[]=$nameerr;
    $err=1;
  } else {
    $name = test_input($_POST["name"]);
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
     $err=1;
      $nameerr = "Only alphabets and spaces allowed";
     $errs[]=$nameerr;
    }
  }

  if (!isset($_POST["events"])) {
    $eventerr = "Event name is required";
    $err=1;
    $errs[]=$eventerr;
  } else {
    $event = test_input($_POST["events"]);
  }
  if (empty($_POST["college"])) {
    $collerr = "College is required";
    $err=1;
    $errs[]=$collerr;
  } else {
    $college = test_input($_POST["college"]);
    if (!preg_match("/^[a-zA-Z .]*$/",$college)) {
     $err=1;
      $collerr = "Only alphabets and . is allowed";
    $errs[]=$collerr;
    }
  }
  if (empty($_POST["phno"])) {
    $phnoerr = "Phone Number is required";
    $err=1;
    $errs[]=$phnoerr;
  } else {
    $phno = test_input($_POST["phno"]);
    if (!preg_match("/^[0-9]{10}$/",$phno)) {
     $err=1;
      $phnoerr = "Enter 10 digit mobile number";
    $errs[]=$phnoerr;
    }
  }





if($err == 0)
{


if(isset($_POST['events']) && isset($_POST['name']) && isset($_POST['college']) && isset($_POST['phno']))
{
	$phno = $_POST['phno'];
	$event = $_POST['events'];
	$name = $_POST['name'];
	$college = $_POST['college'];
	$time = date('Y-m-d H:i:s',time()+19800);
	if(isset($_POST['regandcontinue']))
	{
		$_SESSION['regname']=$name;
		$_SESSION['regphno']=$phno;
		$_SESSION['regcollege']=$college;
	}
	else
	{
		$_SESSION['regname']="";
		$_SESSION['regphno']="";
		$_SESSION['regcollege']="";
	}
	$query1 = "select * from participants where name='$name' and phno='$phno' and event='$event'";
	$q1 = mysqli_query($conn,$query1);
	$count = mysqli_num_rows($q1);
	$registrar_name=$_SESSION['usrname'];
	$unique_id = mt_rand(100,999) . "-" . mt_rand(100,999) . "-" . mt_rand(100,999);
	if($count==0)
	{
		while(true)
		{
			$unique_id = mt_rand(100,999) . "-" . mt_rand(100,999) . "-" . mt_rand(100,999);
			$query4 = "Select * from participants where unique_id='$unique_id'";
			$q4 = mysqli_query($conn,$query4);
			$count2 = mysqli_num_rows($q4);
			if($count2 == 0)
				break;

		}
	if(!isset($_SESSION['usrname']) || !strcmp($_SESSION['usrname'],"") )
	{

		header('Location:/logout.php');
	}

	if(strcmp($registrar_name,"")==0 )
	{

		header('Location:/logout.php');
	}
	$query = "Insert into participants(name,college,phno,event,timestamp,registrar_name,unique_id) values('$name','$college','$phno','$event','$time','$registrar_name','$unique_id')";
	$q = mysqli_query($conn,$query);
	$confirm_query = "SELECT * from participants where name = '$name' and college = '$college' and unique_id='$unique_id'";
	$confirm_q1 = mysqli_query($conn,$confirm_query);
	$row =  mysqli_fetch_assoc($confirm_q1);
	if(strcmp($row['registrar_name'],"") == 0){
		$delete_query = "DELETE from participants where name = '$name' and college = '$college' and unique_id='$unique_id'";
		$delete_q1 = mysqli_query($conn,$delete_query);
		header('Location: /Home');
	}
	$query2 = "Select * from events where event_name='$event'";
	$q2 = mysqli_query($conn,$query2);
	$row2 = mysqli_fetch_assoc($q2);
	$registered = $row2['no_of_registrations'] + 1;
	$query3="update events set no_of_registrations = '$registered' where event_name='$event'";
	$q3=mysqli_query($conn,$query3);
	$msg="Participant registered";
	require './secret.php';
	}


}
}
}
else if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['clear']))
{
	$_SESSION['regname']="";
	$_SESSION['regphno']="";
	$_SESSION['regcollege']="";
	$_SESSION['msg']="";
}
include("../navbar.php");
?>
		<div class="container-fluid" id="wrapper">
<h2>
	Participant Registration
</h2>
	        <div class="row">
	            <div class="col-md-3">
	            </div>
	            <div class="col-md-6"  id="main">
	            <!--add backend here                           -->

	<form name="register" action="/Home/" method="post">
		<input type="text" name="name" value="<?php echo $_SESSION['regname'] ?>" id="field" placeholder="Name" ><br>
		<input type="tel" name="phno"  value="<?php echo $_SESSION['regphno'] ?>" id="field" placeholder="Mobile Number" ><br>
		<input type="text" name="college" value="<?php echo $_SESSION['regcollege'] ?>" id="field" placeholder="College" ><br>
		<span id="event_label">Event : </span><br>
				<select name="events" id="select">
					<?php
						$query = "Select * from events";
						$q = mysqli_query($conn,$query);
						while($row = mysqli_fetch_assoc($q))
						{
							$vacancy = $row['max_registrations'] - $row['no_of_registrations'];
							if($vacancy == 0 )
								$disabled="disabled";
							else
								$disabled="";
							echo "<option value='" . $row['event_name'] . "' ". $disabled .">" . $row['event_name']. "---- Rs.".$row['fee'] ." ---- ". $vacancy . " left</option>";
						}

					?>
  				</select><br>
		<?php
			$_SESSION['regname']="";
			$_SESSION['regphno']="";
			$_SESSION['regcollege']="";
	?>      <br>
		<?php
                                foreach($errs as $fieldError)
                                        echo "
               			 <div class=\"alert alert-danger\">
                        	<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
                        	" . $fieldError ."</div>";
			if($msg != "")
			{
				echo "<div class=\"alert alert-success\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>".$msg."</div>";
				$msg="";
			}
                        ?>
		<div class="alert alert-warning">Double-check that the <strong>mobile number</strong> is correct.</div>
		<div class="alert alert-danger"><strong>Registrations cannot be cancelled.</strong> Be sure before you submit the form.</div>
		<h5><strong>Your last three registrations</strong></h5>
		<?php
			include("../db_connect.php");
			$name = $_SESSION['usrname'];
			$query = "SELECT name from participants where registrar_name='$name' order by timestamp desc";
			$q = mysqli_query($conn,$query);
			$count = 1;
			while($count <= 3){
				$row = mysqli_fetch_assoc($q);
				echo '<span>'.$row["name"].'</span><br>';
				$count = $count + 1;
			}
		?>
		<input type="submit" name="reg" value="Register" id="button">
		<input type="submit" name="regandcontinue" value="Register & Repeat" id="button">
		</form>
		<a href="/"><button value="clear" id="button">Clear</button></a>
	            </div>
	            <div class="col-md-3">

	            </div>
	<div>
	</div>
</body>
</html>
