<?php
$baseUrl = 'https://alerts.sinfini.com/api/v3/index.php';
$params = [
'method' => 'sms',

'api_key' => 'A5e952e0b7bec625b9885a52c4499bb55',
'format' => 'json',
'sender' => 'BMSITM',
'to' => $phno,
'message' => 'Hi '.$name.', you have been successfully registered as event head for '.$event.'

You can download the participants\' list on https://register.bmsit.ac.in/EventHead

Your login credentials are as follows:
Phone Number: '.$phno.'
Password: '.$pwd.'

Note the following points 
-> You can view participants registered in the above mentioned link.
-> You can download/print the final list only after you stop registrations. 
-> You will be held responsible if the event registration is stopped by mistake. Use with caution. 

For any queries contact 8197360630 (Jayasurya)', // message body
];
$url = $baseUrl . '?' . http_build_query($params);
file_get_contents($url);
?>
