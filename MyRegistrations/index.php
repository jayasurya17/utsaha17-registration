<html>
<head>
<title>
	Utsaha - Members
</title>
<?php include("../bootstrap_header.html"); ?>
</head>
<body>
		<?php
			session_start();
			if(!isset($_SESSION['usrname']))
			{
				header('Location:/');
			}
			include("../navbar.php");
		?>
		<div class='container-fluid' id='wrapper'>
	<hr>
	<h1>My Registrations</h1><hr>
	        <div class="row">
	            <div class="col-md-2">
	            </div>
	            <div class="col-md-8"  id="main">
	            <strong>Total amount : <?php
	            	include ("../db_connect.php");
					$member_name=$_SESSION['usrname'];
	            	$query = mysqli_query($conn,"SELECT sum(e.fee) as sum1 from participants p,events e where p.event=e.event_name and p.registrar_name='$member_name'");
	            	$row = mysqli_fetch_assoc($query);
	            	echo $row['sum1'];
	            	?>
	            	</strong>
	            <hr>

	<?php

	include("../db_connect.php");
		$member_name=$_SESSION['usrname'];
		$query = "Select timestamp from participants where registrar_name='$member_name'";
		$q = mysqli_query($conn,$query);
		while($row = mysqli_fetch_assoc($q))
		{
			$date = date( "Y-m-d", strtotime( $row['timestamp'] ) );
			$date_arr[]=$date;
		}
		$date_arr = array_unique($date_arr);
		rsort($date_arr);
		foreach($date_arr as $x)
		{
			$total=0;
			echo "<table class=\" table table-striped table-hover \">
			<tr>
				<th class='regtd'>ID</th>
				<th class='regtd'>Name</th>
				<th class='regtd'>Phone Number</th>
				<th class='regtd'>College</th>
				<th class='regtd'>Event</th>
				<th class='regtd'>Time</th>
				<th class='regtd'>Fee</th>
			</tr>
			";
			$query2 = "Select * from events e,participants p where e.event_name = p.event and p.registrar_name='$member_name' and date(p.timestamp) ='$x'";
			$q2 = mysqli_query($conn,$query2);
			while($row = mysqli_fetch_assoc($q2))
			{
				$total=$total+$row['fee'];
				echo "<tr><td class='regtd'>".$row['id']."</td><td class='regtd'>".$row['name']."</td><td class='regtd'>".$row['phno']."</td><td class='regtd'>".$row['college']."</td><td class='regtd'>".$row['event']."</td><td class='regtd'>".$row['timestamp']."</td><td class='regtd'>".$row['fee']."</td></tr>";
			}
		echo "<tr><td class=\"total\" colspan=7>Total (".$x.") = ".$total."</td></tr>";
		echo "</table><hr>";
		}
?>
	            </div>
	        <div class="col-md-2">
			</div>
	    </div>

</body>
</html>
