<div class="container-fluid">
<nav class="navbar navbar-custom">
		<div class="container-fluid">


        <!-- Logo -->
	        <div class="navbar-header">
	            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavBar">
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>
	            <a href="/Home" class="navbar-brand semHead">Utsaha 2017</a>
	        </div>

	        <!-- Menu Items -->
	        <div class="collapse navbar-collapse" id="mainNavBar">
	            <ul class="nav navbar-nav">
	                <li class="active"><a href="/Home">Register</a></li>
<!---	                <li><a href="/Leaderboard">Leaderboard</a></li> --->
	                <li><a href="/MyRegistrations">My Registrations</a></li> 
			<?php
				if(isset($_SESSION['super']) && $_SESSION['super']==1)
				{
					echo "
	                		<li><a href='/EventDetails'>Event details</a></li>";
				}
			?>
			<li><a href="/Rules">Rules</a></li>
	                <li><a href="/ChangePassword">Change Password</a></li>
			<?php
				if(isset($_SESSION['super']) && $_SESSION['super']==1)
				{
					echo "
			                <li class='dropdown'>
                        		<a href='#' class='dropdown-toggle' data-toggle='dropdown'>Admin Stuff<span class='caret'></span></a>
		                        <ul class='dropdown-menu'>
		                            <li><a href='/Admin/AddMember'>Member Registrations</a></li>
		                            <li><a href='/Admin/AddEvent'>Add Events</a></li>
		                            <li><a href='/Admin/Stats'>Daily Stats</a></li>
					    <li><a href='/Admin/all_participants.php' target='_blank'>All Participants</a></li>
		                        </ul>
					</li>";
				}
			?>
			</ul>
			<ul class="nav navbar-nav navbar-right">
	                <li class="right"><a href="/logout.php">Logout</a></li>
	            </ul>
	        </div>
</nav>
</div>
